# libreoffice

Full-featured office suite. http://libreoffice.org

# Official documentation
* [*Installing several versions of LibreOffice in parallel*
  ](https://wiki.documentfoundation.org/Installing_in_parallel) (also without administration rights)
